CKEditor 5 strapi upload adapter
========================================

## Documentation

This adapter is to be used with Strapi when using CKEditor as the wysiwyg editor.
To enable CKEditor on Strapi, please visit: [https://strapi.io/blog/how-to-change-the-wysiwyg-in-strapi](https://strapi.io/blog/how-to-change-the-wysiwyg-in-strapi)

To enable this upload adapter, you need to override the default CKEditor plugin.
Add this to extentions/content-manager/admin/src/components/CKEditor/index.js

```javascript
import React from 'react';
import PropTypes from 'prop-types';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import styled from 'styled-components';
import { auth } from 'strapi-helper-plugin';
import StrapiUploadAdapter from '@martinkronstad/ckeditor5-strapi-upload-adapter'; 

const Wrapper = styled.div`
  .ck-editor__main {
    min-height: 200px;
    > div {
      min-height: 200px;
    }
  }
`;

const Editor = ({ onChange, name, value }) => {

  const jwtToken = auth.getToken();
  return (
    <Wrapper>
      <CKEditor
        editor={ClassicEditor}
        data={value}
        onChange={(event, editor) => {
          const data = editor.getData();
          onChange({ target: { name, value: data } });
        }}
        config={{
          extraPlugins: [StrapiUploadAdapter],
          strapiUploadAdapter: {
            uploadUrl: `${strapi.backendURL}/upload`,
            absUrl: `${strapi.backendURL}`,
            headers: {
              Authorization: "Bearer " + jwtToken
            }
          }        
        }}
      />
    </Wrapper>
  );
};

Editor.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default Editor;
```

## Options

| Option | Default | Description |
| ------ | ------- | ----------- |
| uploadUrl | null | Is required, this is the path to the upload-service |
| absUrl | null | Is optional, prefixes urls from strapi with an url for absolute urls. |

## License

Licensed under the terms of [GNU General Public License Version 2 or later](http://www.gnu.org/licenses/gpl.html). For full details about the license, please check the `LICENSE.md` file or [https://ckeditor.com/legal/ckeditor-oss-license](https://ckeditor.com/legal/ckeditor-oss-license).
